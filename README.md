# Uhrzeiten 

Dieses Programm stellt verschiedene Zeiten unterschiedlich dar.
Dazu erhält jede Zeitanzeige eine eigene Ansicht (JDialog).

Es wurde die Programmiersprache Java verwendet und das Observer-Entwurfsmuster
angewandt.

Stile:
- Digitaluhr mit 24 Studentakt
- Digitalanzeige als Restzeitanzeige
- Angelsächsische Anzeige mit 12 Stundentakt
- Analog (noch nicht implementiert)

