package main;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import observer.Digitalanzeige;
import observer.EnglischeAnzeige;
import observer.Restzeitdigitalanzeige;

/**
 * 
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 *
 */
public class UhrzeitProgramm {
	
	public static final String digitalFontFamily;
	
	static 
	{	
		Font digitalFont = null;
		try {
			// Setze das Aussehen
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

			// Importiere die Digital-Schriftart
			digitalFont = Font.createFont(Font.TRUETYPE_FONT, UhrzeitProgramm.class.getResourceAsStream("/digital-7.ttf"));
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(digitalFont);
		} catch (Exception e) {
			fehler(e);
		} finally {			
			// setze anschlie�end den Schriftartnamen
			if (digitalFont != null)
			{
				digitalFontFamily = digitalFont.getFamily();
			}
			else
			{
				digitalFontFamily = "";
			}
		}
	}
	
	// Hauptprogramm
	public static void main(String[] args) {
		Systemzeit systemzeit = new Systemzeit();
		
		systemzeit.fuegeAnzeigeHinzu(new Digitalanzeige(new Point(10, 10)));
		systemzeit.fuegeAnzeigeHinzu(new EnglischeAnzeige(new Point(420, 10)));
		systemzeit.fuegeAnzeigeHinzu(new Restzeitdigitalanzeige(new Point(830, 10)));
		// systemzeit.fuegeAnzeigeHinzu(new Analoganzeige(new Point(10, 10)));
		systemzeit.alleAnzeigen();
		
		Timer timer = new Timer();
		/*
		 * Hier wird ein asynchroner Timer mit der 
		 * Periodendauer von einer Sekunde gestartet.
		 * 
		 * Der Timer "erzeugt" sek�ndlich ein neues Ereignis, 
		 * das von den Observern beobachtet wird.
		 */
		timer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				
				if (!systemzeit.dialogSichtbar())
				{
					// Wenn kein Dialog mehr sichtbar ist, dann
					// soll der Timer und das Programm beendet werden.
					this.cancel();
					System.exit(0);
				}
				else
				{		
					// Ereignis erstellen
					systemzeit.zeitAnzeigen();
				}
			}
			
		}, 0L, 1000L);
		
		
		/*
		 *  Ende des Hauptprogramm
		 *  Das Programm l�uft solange weiter, bis alle Fenster und andere
		 *  Threads beendet wurden.
		 */
	}

	/**
	 * Zeigt den Fehler in einem Dialag an und beendet das Programm 
	 * mit den ExitCode -1;
	 * 
	 * @param ex Exception
	 */
	private static void fehler(Exception ex) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		JOptionPane.showMessageDialog(null, sw.toString(), "Fehler", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);		
	}

}
