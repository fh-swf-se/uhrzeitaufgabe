package main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import observer.Zeitanzeige;

/**
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public class Systemzeit extends Subject {
	
	private List<Zeitanzeige> ansichten;
	
	public Systemzeit() {
		this.ansichten = new ArrayList<Zeitanzeige>();
	}
	
	// Daten, die von dem Observer abgefragt werden. 
	public Date getAktuelleZeit() {
		return new Date();
	}

	/**
	 * Benachrichtigt alle eingetragenen Observer.
	 */
	public void zeitAnzeigen(){
		this.notifyObservers();
	}
	
	/**
	 * F�gt eine neue Zeitanzeige als Observer hinzu.
	 * 
	 * @param anzeige Zeitanzeige
	 */
	public void fuegeAnzeigeHinzu(Zeitanzeige anzeige) {
		this.ansichten.add(anzeige);
		this.attach(anzeige);
	}
	
	/**
	 * Zeigt alle Ansichten an.
	 */
	public void alleAnzeigen() {
		this.ansichten.forEach(Zeitanzeige::showDialog);
	}

	/**
	 * Diese Methode pr�ft, ob noch mindestens ein Dialog angezeigt wird.
	 */
	public boolean dialogSichtbar() {
		return this.ansichten.stream()
			.filter(Zeitanzeige::isVisible).count() > 0;
	}
	
}