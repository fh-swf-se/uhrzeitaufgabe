package main;

import java.util.ArrayList;
import java.util.List;

import observer.Observer;

/**
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public class Subject {

	private List<Observer> observerList;

	public Subject() {
		this.observerList = new ArrayList<Observer>();
	}
	
	/**
	 * 
	 * @param o
	 */
	public void attach(Observer o)
	{
		this.observerList.add(o);
	}

	/**
	 * 
	 * @param o
	 */
	public void detach(Observer o)
	{
		this.observerList.remove(o);
	}

	public void notifyObservers()
	{
		this.observerList.forEach((o) -> {
			o.update(this);
		});
	}

}