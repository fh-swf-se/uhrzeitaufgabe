package observer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JDialog;

/**
 * 
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 * 
 */
public abstract class Zeitanzeige extends JDialog implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3413516321616266085L;

	/**
	 * 
	 * @param title Texttitel des Dialogs in der Fensterleiste
	 * @param location Position auf dem Bildschirm
	 */
	public Zeitanzeige(String title, Point location)
	{
		this.preInit(title, location);
	}

	private final void preInit(String title, Point location)
	{
		// Standardgr��e festlegen
		this.setPreferredSize(new Dimension(400, 200));	

		this.initDialog();
				
		this.setTitle(title);
		this.setResizable(false);
		this.pack();
		this.setLocation(location);
	}
	
	/**
	 * Diese Methode zeigt den Dialog am dem Bildschirm an.
	 */
	public final void showDialog()
	{
		this.setVisible(true);
	}
	
	/**
	 * Methode zum Initialisieren des Dialogs.
	 * Standardm��ig wird das BorderLayout und die Hintergrundfarbe auf Schwarz gesetzt.
	 * 	 
	 * Diese Methode ist zum �berschreiben gedacht.
	 */
	protected void initDialog()
	{
		this.setLayout(new BorderLayout());
		this.getContentPane().setBackground(Color.BLACK);
	}
	
	
}
