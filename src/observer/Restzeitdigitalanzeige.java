package observer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JLabel;

import main.Subject;
import main.Systemzeit;
import main.UhrzeitProgramm;

/**
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public class Restzeitdigitalanzeige extends Zeitanzeige {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -1342906739829957543L;
	
	private SimpleDateFormat dateFormat;
	private JLabel anzeigeLabel;
	
	private TimeZone timeZone;
	
	public Restzeitdigitalanzeige(Point location) {
		super("Restzeitdigitalanzeige", location);
	}
	
	@Override
	protected void initDialog() {
		super.initDialog();

		this.timeZone = TimeZone.getTimeZone("UTC");
		
		this.dateFormat = new SimpleDateFormat("HH:mm:ss");
		this.dateFormat.setTimeZone(this.timeZone);
		
		Font digitalFont = new Font(UhrzeitProgramm.digitalFontFamily, Font.PLAIN, 60);
		this.anzeigeLabel = new JLabel("00:00:00", JLabel.CENTER);
		this.anzeigeLabel.setFont(digitalFont);
		this.anzeigeLabel.setForeground(Color.RED);
		
		this.add(this.anzeigeLabel, BorderLayout.CENTER);
	}

	/**
	 * 
	 * @param s
	 */
	public void update(Subject s) {
		Systemzeit sz = (Systemzeit) s;
	
		Date date = sz.getAktuelleZeit();
		long restZeit = 86400000L - (date.getTime() % 86400000L);
		if (!this.timeZone.inDaylightTime(date))
		{
			restZeit = restZeit - 3600000L;
		}
		this.anzeigeLabel.setText(this.dateFormat.format(restZeit));
	}
}