package observer;

import java.awt.Point;

import main.Subject;
import main.Systemzeit;

/**
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public class Analoganzeige extends Zeitanzeige {

	/**
	 * generated
	 */
	private static final long serialVersionUID = 3734704132462801168L;

	public Analoganzeige(Point location) {
		super("Analoganzeige", location);
	}

	@Override
	protected void initDialog() {
		// TODO
	}

	/**
	 * 
	 * @param s
	 */
	public void update(Subject s) {
		Systemzeit sz = (Systemzeit) s;
	}

}