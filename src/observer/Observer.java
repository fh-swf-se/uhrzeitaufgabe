package observer;

import main.Subject;

/**
 * @author Joel-FH
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public interface Observer {

	/**
	 * 
	 * @param s
	 */
	public void update(Subject s);

}