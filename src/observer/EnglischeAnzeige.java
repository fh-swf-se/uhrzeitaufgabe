package observer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.text.SimpleDateFormat;

import javax.swing.JLabel;

import main.Subject;
import main.Systemzeit;
import main.UhrzeitProgramm;

/**
 * @author Joel Steffens
 * @version 1.0
 * 
 * @created 04.12.2019
 */
public class EnglischeAnzeige extends Zeitanzeige {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -1342906739829957543L;
	
	private SimpleDateFormat dateFormat;
	private JLabel anzeigeLabel;
	
	public EnglischeAnzeige(Point location) {
		super("Englische Formatanzeige", location);
	}
	
	@Override
	protected void initDialog() {
		super.initDialog();

		this.dateFormat = new SimpleDateFormat("KK:mm:ss a");
		
		Font digitalFont = new Font(UhrzeitProgramm.digitalFontFamily, Font.PLAIN, 60);
		this.anzeigeLabel = new JLabel("00:00:00 AM", JLabel.CENTER);
		this.anzeigeLabel.setFont(digitalFont);
		this.anzeigeLabel.setForeground(Color.RED);
		
		this.add(this.anzeigeLabel, BorderLayout.CENTER);
	}
	
	/**	
	 * 
	 * @param s
	 */
	public void update(Subject s) {
		Systemzeit sz = (Systemzeit) s;
		this.anzeigeLabel.setText(this.dateFormat.format(sz.getAktuelleZeit()));
	}
	
}